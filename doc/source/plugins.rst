Plugins
=======

Discovery
+++++++++


Zeroconf
--------

.. automodule:: beiran_discovery_zeroconf.zeroconf
    :members:
    :undoc-members:
    :show-inheritance:
    :special-members: __init__


DNS
---

.. automodule:: beiran_discovery_dns.dns
    :members:
    :undoc-members:
    :show-inheritance:
    :special-members: __init__



Package
+++++++


Docker
------

.. automodule:: beiran_package_docker.plugin_docker
    :members:
    :undoc-members:
    :show-inheritance:
    :special-members: __init__


.. automodule:: beiran_package_docker.api
    :members:
    :undoc-members:
    :show-inheritance:
    :special-members: __init__



Interface
+++++++++


Kubernetes
----------

.. automodule:: beiran_interface_k8s.k8s
    :members:
    :undoc-members:
    :show-inheritance:
    :special-members: __init__
