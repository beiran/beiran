# Beiran P2P Package Distribution Layer
# Copyright (C) 2019  Rainlab Inc & Creationline, Inc & Beiran Contributors
#
# Rainlab Inc. https://rainlab.co.jp
# Creationline, Inc. https://creationline.com">
# Beiran Contributors https://docs.beiran.io/contributors.html
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

"""
Beiran Docker Plugin command line interface module
"""

import asyncio
import sys
# import progressbar
import click
from tabulate import tabulate

from beiran.util import json_streamer
from beiran.util import sizeof_fmt
from beiran.multiple_progressbar import MultipleProgressBar
from beiran.cli import pass_context
from .util import DockerUtil


@click.group()
def cli():
    """Docker Commands

    Manage your docker images and layers in cluster.

    Please see sub-commands help texts."""
    pass


@cli.group("image", short_help="docker image subcommand")
def image():
    """Manage Docker Images

    List and pull docker images.
    """
    pass

async def _pull_with_progress(ctx, imagename, node, force):
    """Pull image with async client (whole image)"""
    progbar = MultipleProgressBar(desc=imagename)
    resp = await ctx.async_beiran_client.pull_image(
        imagename,
        node=node,
        wait=True,
        force=force,
        whole_image_only=True,
        progress=True
    )
    is_finished = False
    async for update in json_streamer(resp.content, '$.progress[::]'):
        err = update.get('error')
        if err:
            click.echo('An error occured while pulling the image. {}'.format(err))
            return 1
        if update.get('finished'):
            click.echo("Image pulling process finished.")
            is_finished = True
            break
        if update['status'] == DockerUtil.DL_ALREADY:
            click.echo("%s Already exists"%update['digest'])
            is_finished = True
            break
        progbar.update(update['progress'])
    progbar.finish()

    if not is_finished:
        click.echo('An error occured!')
        return 1

async def _pull_with_progress_distributed(ctx, imagename, node, force):
    """Pull image with async client (distributed)"""
    progbars = {}
    resp = None
    try:
        resp = await ctx.async_beiran_client.pull_image(
            imagename,
            node=node,
            wait=True,
            force=force,
            whole_image_only=False,
            progress=True
        )
    except Exception as err: # pylint: disable=broad-except
        click.echo('An exception is catched while requesting pull image!')
        click.echo(str(err))
        return 1

    click.echo('Downloading layers...')
    lastbar = None

    async for data in json_streamer(resp.content, '$.progress[::]'):
        resp_err = data.get('error')
        if resp_err:
            for progbar in list(progbars.values()):
                progbar['bar'].finish(dirty=True)
            if lastbar:
                lastbar.seek_last_line()
            click.echo('An error occured while pulling the image. {}'.format(resp_err))
            return 1
        if data.get('finished'):
            if lastbar:
                lastbar.seek_last_line()
            click.echo("Image pulling process finished.")
            return
        if data.get('status') == DockerUtil.DL_ALREADY:
            click.echo("%s Already exists"%data['digest'])
            return

        digest = data['digest']

        if digest not in progbars:
            if data['status'] == DockerUtil.DL_ALREADY:
                progbars[digest] = {
                    'bar': MultipleProgressBar(
                        widgets=[digest + ' Already exists']
                    )
                }
            else:
                progbars[digest] = {
                    'bar': MultipleProgressBar(desc=digest)
                }
        progbars[digest]['bar'].update_and_seek(data['progress'])
        lastbar = progbars[digest]['bar']

    if lastbar:
        lastbar.seek_last_line()
    click.echo('An error occured!')
    return 1

async def _pull_without_progress(ctx, imagename, node, wait, force, whole_image_only): # pylint: disable=too-many-arguments
    """Pull image with async client"""
    try:
        click.echo("Requesting image pull...")
        resp = await ctx.async_beiran_client.pull_image(
            imagename,
            node=node,
            wait=wait,
            force=force,
            whole_image_only=whole_image_only,
            progress=False
        )
    except Exception as err: # pylint: disable=broad-except
        click.echo('An exception is catched while requesting pull image!')
        click.echo(str(err))
        return 1

    async for data in json_streamer(resp.content, '$.status[::]'):
        resp_err = data.get('error')
        if resp_err:
            click.echo('An error occured while pulling the image. {}'.format(resp_err))
            return 1
        if data.get('started'):
            click.echo("Image pulling process started.")
            if not wait:
                return
        if data.get('status'):
            click.echo(data.get('status'))
            return
        if data.get('finished'):
            click.echo("Image pulling process finished.")
            return
    click.echo('An error occured while pulling image!')
    return 1


@image.command('pull')
@click.option('--from', 'node', default=None,
              help='Pull from spesific node')
@click.option('--wait', 'wait', default=False, is_flag=True,
              help='Wait the result of pulling image')
@click.option('--force', 'force', default=False, is_flag=True,
              help='Forces download of image even if the node is not recognised')
@click.option('--no-progress', 'noprogress', default=False, is_flag=True,
              help='Disable image transfer progress display')
@click.option('--whole-image-only', 'whole_image_only', default=False, is_flag=True,
              help='Pull an image from other node (not each layer)')
@click.argument('imagename')
@click.pass_obj
@pass_context
# pylint: disable-msg=too-many-arguments
def image_pull(ctx, node: str, wait: bool, force: bool, noprogress: bool,
               whole_image_only: bool, imagename: str):
    """Pull a container image from cluster or repository"""
    click.echo(
        'Pulling image %s from %s!' % (imagename, node or "available nodes"))
    loop = asyncio.get_event_loop()

    if not noprogress:
        if whole_image_only:
            return_value = loop.run_until_complete(_pull_with_progress(ctx, imagename, node, force))
        else:
            return_value = loop.run_until_complete(
                _pull_with_progress_distributed(ctx, imagename, node, force))
    else:
        return_value = loop.run_until_complete(
            _pull_without_progress(ctx, imagename, node, wait, force, whole_image_only))
    sys.exit(return_value)

# pylint: enable-msg=too-many-arguments


@image.command('list')
@click.option('--all', 'all_nodes', default=False, is_flag=True,
              help='List images from all known nodes')
@click.option('--digests', default=False, is_flag=True,
              help='Show image digests')
@click.option('--node', default=None,
              help='List images from specific node')
@click.pass_obj
@pass_context
def image_list(ctx, all_nodes: bool, digests: bool, node: str):
    """List container images across the cluster"""

    def _get_availability(i):
        if i['availability'] == 'available':
            num = str(len(i['available_at']))
            return i['availability'] + '(' + num + ' node(s))'
        return i['availability']

    images = ctx.beiran_client.get_images(all_nodes=all_nodes, node_uuid=node)

    if digests:
        table = [
            [",\n".join(i['tags']), ",\n".join(i['repo_digests']), i['hash_id'],
             sizeof_fmt(i['size']), _get_availability(i)]
            for i in images
        ]
        headers = ["Tags", "Digests", "ID", "Size", "Availability"]
    else:
        table = [
            [",\n".join(i['tags']), i['hash_id'], sizeof_fmt(i['size']), _get_availability(i)]
            for i in images
        ]
        headers = ["Tags", "ID", "Size", "Availability"]

    click.echo(tabulate(table, headers=headers))


# ##########  Layer management commands

@cli.group("layer")
def layer():
    """Manage Docker Layers"""
    pass


@layer.command('list')
@click.option('--all', 'all_nodes', default=False, is_flag=True,
              help='List layers from all known nodes')
@click.option('--diffid', default=False, is_flag=True,
              help="Show layer's diffid")
@click.option('--node', default=None,
              help='List layers from specific node')
@click.pass_obj
@pass_context
def layer_list(ctx, all_nodes: bool, diffid: bool, node: str):
    """List container layers across the cluster"""
    layers = ctx.beiran_client.get_layers(all_nodes=all_nodes, node_uuid=node)
    if diffid:
        table = [
            [i['digest'], i['diff_id'], sizeof_fmt(i['size']), str(len(i['available_at'])) +
             ' node(s)']
            for i in layers
        ]
        headers = ["Digest", "DiffID", "Size", "Availability"]
    else:
        table = [
            [i['digest'], sizeof_fmt(i['size']), str(len(i['available_at'])) + ' node(s)']
            for i in layers
        ]
        headers = ["Digest", "Size", "Availability"]
    print(tabulate(table, headers=headers))
