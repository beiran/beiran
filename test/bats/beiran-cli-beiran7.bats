@test "pulling an image from a specific node (with '--whole-image-only' and '--from' options) exits peacefully" {
    current_node_uuid="$(beiran node info | tail -1 | awk '{print $2}' | cut -d "#" -f 2)"
    another_node_uuid="$(beiran node list | cut -d ' ' -f1 | grep -x '[_[:alnum:]]\{32\}' | sed '1q;d')"
    if [ "$current_node_uuid" == "$another_node_uuid" ]
    then
    	another_node_uuid="$(beiran node list | cut -d ' ' -f1 | grep -x '[_[:alnum:]]\{32\}' | sed '2q;d')"
    fi
    run beiran docker image pull alpine --whole-image-only --from $another_node_uuid
    [ "$status" -eq 0 ]
}

@test "pulling a non-existing image from a specific node (with '--whole-image-only' and '--from' options) exits with error code" {
    current_node_uuid="$(beiran node info | tail -1 | awk '{print $2}' | cut -d "#" -f 2)"
    another_node_uuid="$(beiran node list | cut -d ' ' -f1 | grep -x '[_[:alnum:]]\{32\}' | sed '1q;d')"
    if [ "$current_node_uuid" == "$another_node_uuid" ]
    then
    	another_node_uuid="$(beiran node list | cut -d ' ' -f1 | grep -x '[_[:alnum:]]\{32\}' | sed '2q;d')"
    fi
    run beiran docker image pull a_non_existent_image --whole-image-only --from $another_node_uuid
    [ "$status" -eq 1 ]
}

@test "pulling an image with docker image manifest v2 schema 1 from another node exists peacefully" {
    run beiran docker image pull alpine:2.7
    [ "$status" -eq 0 ]
}