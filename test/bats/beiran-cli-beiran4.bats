#!/usr/bin/env bats

@test "pulling an image from another node exits peacefully" {
    run beiran docker image pull alpine
    [ "$status" -eq 0 ]
}

@test "pulling a non-existing image exits with error code" {
	run beiran docker image pull a_non_existent_image
	[ "$status" -eq 1 ]
}
