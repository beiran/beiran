#!/usr/bin/env bats

@test "pulling an image from another node (with '--whole-image-only' option) exits peacefully" {
    run beiran docker image pull alpine --whole-image-only
    [ "$status" -eq 0 ]
}

@test "pulling a non-existing image (with '--whole-image-only' option) exits with error code" {
    run beiran docker image pull a_non_existent_image --whole-image-only
    [ "$status" -eq 1 ]
}
