#!/usr/bin/env bats

@test "pulling an image from another node (with '--wait' and '--no-progress' options) exits peacefully" {
    run beiran docker image pull alpine --wait --no-progress
    [ "$status" -eq 0 ]
}

@test "pulling a non-existing image (with '--wait' and '--no-progress' options) exits with error code" {
	run beiran docker image pull a_non_existent_image --wait --no-progress
	[ "$status" -eq 1 ]
}
