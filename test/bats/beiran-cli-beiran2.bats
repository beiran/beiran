#!/usr/bin/env bats

@test "pulling an image from another node (with '--no-progress' option) exits peacefully" {
    run beiran docker image pull alpine --no-progress
    [ "$status" -eq 0 ]
}

@test "pulling a non-existing image (with '--no-progress' option) exits with error code" {
	run beiran docker image pull a_non_existent_image --no-progress
	[ "$status" -eq 1 ]
}

@test "pulling an image with docker image manifest v2 schema 1 from origin exists peacefully" {
	run beiran docker image pull alpine:2.7
	[ "$status" -eq 0 ]
}