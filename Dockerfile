FROM golang:1.12.5-stretch AS tar-split-builder
WORKDIR /opt
RUN git clone https://github.com/vbatts/tar-split.git
RUN go mod init github.com/vbatts/tar-split/cmd/tar-split
RUN echo "require github.com/urfave/cli v1.22.1" >> go.mod
RUN go build -o /tar-split -a -ldflags '-extldflags "-static"' ./tar-split/cmd/tar-split

FROM python:3.6-stretch
LABEL maintainer="info@beiran.io"
RUN apt-get update && apt-get -y install \
	--no-install-recommends \
	python3-pip git curl make libsqlite3-dev libyajl-dev libyajl2

COPY beiran /opt/beiran/beiran
COPY README.md /opt/beiran/README.md
COPY setup.py /opt/beiran/setup.py
COPY plugins/beiran_package_docker /opt/beiran_package_docker/
COPY --from=tar-split-builder /tar-split /opt/beiran_package_docker/beiran_package_docker/tar-split
COPY plugins/beiran_interface_k8s /opt/beiran_interface_k8s/
COPY beiran/config.toml /etc/beiran/config.toml

WORKDIR /opt/beiran
RUN python setup.py install

WORKDIR /opt/beiran_package_docker
RUN python setup.py install

WORKDIR /opt/beiran_interface_k8s
RUN python setup.py install
WORKDIR /opt/beiran

ENV PYTHONPATH=/opt/beiran:/opt/beiran_package_docker:/opt/beiran_interface_k8s

VOLUME /var/lib/beiran
VOLUME /etc/beiran
